<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <meta name="robots" content="index, follow">
    <meta name="description" content="@yield('description')" />
    <meta name="keywords" content="Online Casino, Sports Betting, Gambling, Football, Asian Player" />
    <meta name="author" content="{{ $host_name }}" />

    <!-- google verify -->
    <meta name="google-site-verification" content="1p9E4MM0E7fgKSiMFeem0R8t1S4zEihHexJoAzxHMIg" />

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="HDCasinoAsia" />
    <meta itemprop="description" content="@yield('description')" />
    <meta itemprop="image" content="{{ asset('img/hdcasino-512x512.jpg') }}" />

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@HDCasinoAsia" />
    <meta name="twitter:creator" content="@HDCasinoAsia" />
    <meta name="twitter:domain" content="{{ Request::root() }}" />
    <meta name="twitter:url" content="{{ url()->current() }}" />
    <meta name="twitter:title" content="@yield('title-social')" />
    <meta name="twitter:description" content="@yield('description')" />
    <meta name="twitter:image" content="{{ asset('img/hdcasino-512x512.jpg') }}" />

    <!-- FB meta -->
    <meta property="fb:pages" content="107012457698844" />
    <meta property="fb:app_id" content="921181568397698" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:title" content="@yield('title-social')" />
    <meta property="og:description" content="@yield('description')" />
    <meta property="og:image" content="{{ asset('img/hdcasino-512x512.jpg') }}" />

    <!-- Canonical -->
    <link rel="canonical" href="{{ url()->current() }}" />

    <!-- app -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- style -->
    <link href="{{ asset('css/common.css?v='.$version) }}" rel="stylesheet" type="text/css" />

    <!-- page style -->
    @yield('css')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167328545-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-167328545-1');
    </script>

    <!-- share social button -->
{{--    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5a90e413992ac400137609bf&product=inline-share-buttons' async='async'></script>--}}

</head>
<body>
        <header>
            @include('templates.header')
        </header>

        <!-- banner slide -->
        @yield('banner')

        <!-- content -->
        @yield('content')

        <footer>
            @include('templates.footer')
        </footer>

        <!-- Import JS -->
        <script src="{{ asset('js/app.js') }}" ></script>
        <script src="{{ asset('js/blazy.min.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('js/common.js?v='.$version) }}"></script>
        <!-- script page -->
        @yield('script')
</body>
</html>
