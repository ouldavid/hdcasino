<div class="container-fluid p-0 bg-black">
    <div class="animation-element"></div>
    <div class="container py-md-3 py-0">
        <div class="row">
            <div class="col-md-12">
                <div id="logo" class="d-none d-md-block text-center">
                    <a href="/"><img class="img-fluid" src="{{ asset('img/logo.png') }}" alt="{{ $host_name }}" /></a>
                </div>
            </div>
        </div>
    </div>
</div>
<nav id="navbar" class="navbar navbar-expand-md navbar-dark bg-dark sidebarNavigation" data-sidebarClass="navbar-dark bg-dark">
    <div class="container-fluid">
        <a href="/" class="m-logo d-md-none mb-0">
            <img class="img-fluid" src="{{ asset('img/logo.png') }}" alt="{{ $host_name }}" />
        </a>
        <button class="navbar-toggler leftNavbarToggler" type="button" data-toggle="" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <button type="button" class="close d-md-none" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <a href="/" class="second-brand d-flex justify-content-center d-md-none mb-3">
                <img class="img-fluid" src="{{ asset('img/logo-nav.png') }}" alt="{{ $host_name }}" />
            </a>
            <ul class="nav navbar-nav nav-flex-icons m-auto">
                <li class="nav-item pl-0">
                    <a class="nav-link pl-0" href="/">
                        <div>
                            <i class="{{ config('global.icon_home') }}"></i>
                            <span data-hover="Home">Home</span>
                        </div>
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/1xbet">
                        <div>
                            <span data-hover="1xBet">1xBet</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/188bet">
                        <div>
                            <span data-hover="188Bet">188Bet</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/22bet">
                        <div>
                            <span data-hover="22Bet">22Bet</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/855play">
                        <div>
                            <span data-hover="855Play">855Play</span>
                        </div>
                    </a>
                </li>
            </ul><!-- .navbar-nav -->
        </div><!-- #navbarsExampleDefault -->
    </div>
</nav>

<!-- search overlay -->
<div class="search-overlay">
    <button id="close-btn" type="button" class="close" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <form action="">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-sm-12 offset-sm-0">
                    <div class="input-group form-btn">
                        <input type="text" name="txt_search" class="form-control border-bottom" placeholder="Search here..." aria-describedby="button-submit">
                        <div class="input-group-append">
                            <button class="btn btn-outline-light brd-left-none border-bottom" type="button" id="button-submit"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

