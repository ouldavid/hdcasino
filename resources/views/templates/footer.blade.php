<div id="footer" class="container-fluid py-4 px-0">
    <div class="copy">
        <p class="text-white text-center text-capitalize mb-0">&copy; Copyright {{ $host_name }} 2020, All Rights reserve. | 18+</p>
    </div>
</div><!-- Footer -->
