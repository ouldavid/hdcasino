@extends('layouts.master')

@section('title', ucfirst(strtolower(Route::currentRouteName())).' | Online Sports Betting, Live Betting, Best Odds')
@section('title-social', 'Online Sports Betting, Live Betting, Best Odds')
@section('description', '22Bet Company is an Online Sports betting and Live betting. Bet on football, hockey, tennis, boxing, and other sports online.')

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="container my-4">
        <div class="row">
            <div class="col-lg-6 col-12 position-relative mb-4">
                <div class="img-over">
                    <a href="https://refpasrasw.world/L?tag=d_545677m_7669c_&site=545677&ad=7669" target="_blank" class="img-wrap-lazy">
                        <img class="img-fluid rounded-10 b-lazy" data-src="img/22bet/22bet.jpg" alt="22Bet">
                    </a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="https://refpasrasw.world/L?tag=d_545677m_7669c_&site=545677&ad=7669" target="_blank" class="btn btn-sm register bg-red-gradient">Play Now</a>
                </div>
            </div>
            <div class="col-lg-6 col-12 bg-black mb-4 p-4">
                <h4 class="text-uppercase text-center text-white">22Bet</h4>
                <p class="text-center text-white-50 font-14">
                    <span class="color-gold">WELCOME BONUS UP TO $122</span> FOR SPORTS BETTING <br/>
                    Get your $122 welcome bonus right now and appreciate the variety of betting opportunities on 22Bet.
                </p>
                <p class="text-center text-white font-14">Enter this promo code <span class="promote-code color-gold">22_2085</span> to get more bonus</p>
                <div class="d-flex justify-content-center">
                    <a href="https://refpasrasw.world/L?tag=d_545677m_7669c_&site=545677&ad=7669" target="_blank" class="btn register bg-red-gradient position-relative position-b-0 mt-lg-3">Play Now</a>
                </div>
            </div>
        </div>
        <div class="mt-3">
            <h4 class="text-uppercase text-center text-white">Online Sports Betting, Live Betting, Best Odds</h4>
            <p class="text-center text-white-50 font-14">
                22Bet Company is an Online Sports betting and Live betting.<br>
                Bet on football, hockey, tennis, boxing, and other sports online.
            </p>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
{{--    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
