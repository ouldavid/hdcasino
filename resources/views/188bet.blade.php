@extends('layouts.master')

@section('title', ucfirst(strtolower(Route::currentRouteName())).' | Online Sportsbetting and Live Casino')
@section('title-social', 'Online Sportsbetting and Live Casino')
@section('description', '188BET offers one among the foremost rewarding sports betting, live casino and online gambling experience. Create your account and begin winning today!!!')

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="container my-4">
        <div class="row ad-img">
            <div class="col-12 text-center mb-4">
                <script type="text/javascript"> var t = new Date(); var img = "<a href='https://aff.188bet.com/Accept.ashx?sid=f7f71df6-d517-48e2-84b0-08ea558adc08&lang=en-gb&aid=29954&cid=8a177909-6707-45fd-a7c1-191ed500d9f3'><img src='https://aff.188bet.com/Collateral.ashx?sid=f7f71df6-d517-48e2-84b0-08ea558adc08&lang=en-gb&id=8a177909-6707-45fd-a7c1-191ed500d9f3&t=" + t.getTime() + "' border='0'></a>"; document.write(img); </script>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-12 position-relative mb-4">
                <div class="img-over">
                    <a href="https://aff.188bet.com/29954/2020" target="_blank" class="img-wrap-lazy">
                        <img class="img-fluid rounded-10 b-lazy" data-src="img/188bet/188bet.jpg" alt="188bet">
                    </a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="https://aff.188bet.com/29954/2020" target="_blank" class="btn btn-sm register bg-red-gradient">Play Now</a>
                </div>
            </div>
            <div class="col-lg-6 col-12 bg-black mb-4 p-4">
                <h4 class="text-uppercase text-center text-white">188BET</h4>
                <p class="text-center text-white-50 font-14">
                    Enjoy a <span class="color-gold">100% Welcome Bonus</span> of up to USD 100 when you deposit for the first time with 188BET! Join 188BET today! Full terms and conditions apply.
                </p>
                <p class="text-center text-white font-14">Enter this promo code <span class="promote-code color-gold">29954</span> to get more bonus</p>
                <div class="d-flex justify-content-center">
                    <a href="https://aff.188bet.com/29954/2020" target="_blank" class="btn register bg-red-gradient position-relative position-b-0 mt-lg-3">Play Now</a>
                </div>
            </div>
        </div>
        <div class="mt-3">
            <h4 class="text-uppercase text-center text-white">Online Sportsbetting and Live Casino</h4>
            <p class="text-center text-white-50 font-14">
                188BET offers one among the foremost rewarding sports betting, live casino and online gambling experience.<br>
                Create your account and begin winning today!!!
            </p>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
{{--    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
