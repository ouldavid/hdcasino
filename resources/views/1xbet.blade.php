@extends('layouts.master')

@section('title', ucfirst(strtolower(Route::currentRouteName())).' | The best online sports betting in Asia')
@section('title-social', 'The best online sports betting in Asia')
@section('description', 'Place bets with a reliable betting company! Live and pre-match sports bets. The best odds and fantastic bonuses. Place bets and win big with 1xBet.')

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="container my-4">
        <div class="row">
            <div class="col-lg-6 col-12 position-relative mb-4">
                <div class="img-over">
                    <a href="http://refpahpgvvad.top/L?tag=d_545679m_2365c_&site=545679&ad=2365" target="_blank" class="img-wrap-lazy">
                        <img class="img-fluid rounded-10 b-lazy" data-src="img/1xbet/1xbet.jpg" alt="1xbet">
                    </a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="http://refpahpgvvad.top/L?tag=d_545679m_2365c_&site=545679&ad=2365" target="_blank" class="btn btn-sm register bg-red-gradient">Play Now</a>
                </div>
            </div>
            <div class="col-lg-6 col-12 bg-black mb-4 p-4">
                <h4 class="text-uppercase text-center text-white">1xBet</h4>
                <p class="text-center text-white-50 font-14">
                    <span class="color-gold">100% BONUS</span> ON THE FIRST DEPOSIT UP TO $100<br>
                    Register with 1xBet and receive a 100% bonus to the maximum amount of $100 on the first deposit!
                </p>
                <p class="text-center text-white font-14">Enter this promo code <span class="promote-code color-gold">1x_176796</span> to get more bonus</p>
                <div class="d-flex justify-content-center">
                    <a href="http://refpahpgvvad.top/L?tag=d_545679m_2365c_&site=545679&ad=2365" target="_blank" class="btn register bg-red-gradient position-relative position-b-0 mt-lg-3">Play Now</a>
                </div>
            </div>
        </div>
        <div class="mt-3">
            <h4 class="text-uppercase text-center text-white">The best online sports betting in Asia</h4>
            <p class="text-center text-white-50 font-14">
                Place bets with a reliable betting company! Live and pre-match sports bets.<br>
                The best odds and fantastic bonuses. Place bets and win big with 1xBet.
            </p>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
{{--    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
