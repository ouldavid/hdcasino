@extends('layouts.master')

@section('title', ucfirst(strtolower(Route::currentRouteName())).' | The Best online casino, sports betting, lottery, and slot machine in Cambodia')
@section('title-social', 'The Best online casino, sports betting, lottery, and slot machine in Cambodia')
@section('description', '855Play is an excellent website most popular with many players. We provide players with comprehensive services such as football, gambling, sports, online live games, etc., and obtain bonuses once you open an account.')

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="container my-4">
        <div class="row">
            <div class="col-lg-6 col-12 position-relative mb-4">
                <div class="img-over">
                    <a href="https://www.855play.com/index.jsp?param=Registration&AgentID=141956" target="_blank" class="img-wrap-lazy">
                        <img class="img-fluid rounded-10 b-lazy" data-src="img/855play/855play.jpg" alt="855play">
                    </a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="https://www.855play.com/index.jsp?param=Registration&AgentID=141956" target="_blank" class="btn btn-sm register bg-red-gradient">Play Now</a>
                </div>
            </div>
            <div class="col-lg-6 col-12 bg-black mb-4 p-4">
                <h4 class="text-uppercase text-center text-white">855Play</h4>
                <p class="text-center text-white-50 font-14">
                    <span class="color-gold">Cash Rebate 5% Per Month 10$ up to 855$</span> Play with website <a href="https://www.855play.com/index.jsp?param=Registration&AgentID=141956">www.855play.com</a> and enjoy all game can get Cash rebate 5% per month.
                </p>
                <div class="d-flex justify-content-center">
                    <a href="https://www.855play.com/index.jsp?param=Registration&AgentID=141956" target="_blank" class="btn register bg-red-gradient position-relative position-b-0 mt-lg-3">Play Now</a>
                </div>
            </div>
        </div>
        <div class="mt-3">
            <h4 class="text-uppercase text-center text-white">The Best online casino, sports betting, lottery, and slot machine in Cambodia</h4>
            <p class="text-center text-white-50 font-14">
                855Play is an excellent website most popular with many players.<br>
                We provide players with comprehensive services such as football, gambling, sports, online live games, etc., and obtain bonuses once you open an account.
            </p>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
{{--    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
