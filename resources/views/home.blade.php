@extends('layouts.master')

@section('title', $host_name.' | The Best Guide to Asian Online Casino and Sports Betting')
@section('title-social', 'The Best Guide to Asian Online Casino and Sports Betting')
@section('description', 'HD Casino assist you to seek out the simplest Online Casinos and Sports Betting! Easy to play games and bet to win real money with licensed, legal, and legit online casinos and sports betting websites for Asia players.')

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="container my-4">
        <div class="row">
            <div class="col-md-4 col-12 position-relative mb-4">
                <div class="img-over">
                    <a href="http://refpahpgvvad.top/L?tag=d_545679m_2365c_&site=545679&ad=2365" target="_blank" class="img-wrap-lazy">
                        <img class="img-fluid rounded-10 b-lazy" data-src="img/1xbet/1xbet.jpg" alt="1xBet">
                    </a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="http://refpahpgvvad.top/L?tag=d_545679m_2365c_&site=545679&ad=2365" target="_blank" class="btn btn-sm register bg-red-gradient">Play Now</a>
                </div>
            </div>
            <div class="col-md-4 col-12 position-relative mb-4">
                <div class="img-over">
                    <a href="https://aff.188bet.com/29954/2020" target="_blank" class="img-wrap-lazy">
                        <img class="img-fluid rounded-10 b-lazy" data-src="img/188bet/188bet.jpg" alt="188Bet">
                    </a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="https://aff.188bet.com/29954/2020" target="_blank" class="btn btn-sm register bg-red-gradient">Play Now</a>
                </div>
            </div>
            <div class="col-md-4 col-12 position-relative mb-4">
                <div class="img-over">
                    <a href="https://refpasrasw.world/L?tag=d_545677m_7669c_&site=545677&ad=7669" target="_blank" class="img-wrap-lazy">
                        <img class="img-fluid rounded-10 b-lazy" data-src="img/22bet/22bet.jpg" alt="22Bet">
                    </a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="https://refpasrasw.world/L?tag=d_545677m_7669c_&site=545677&ad=7669" target="_blank" class="btn btn-sm register bg-red-gradient">Play Now</a>
                </div>
            </div>
            <div class="col-md-4 col-12 position-relative mb-4">
                <div class="img-over">
                    <a href="https://www.855play.com/index.jsp?param=Registration&AgentID=141956" target="_blank" class="img-wrap-lazy">
                        <img class="img-fluid rounded-10 b-lazy" data-src="img/855play/855play.jpg" alt="855Play">
                    </a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="https://www.855play.com/index.jsp?param=Registration&AgentID=141956" target="_blank" class="btn btn-sm register bg-red-gradient">Play Now</a>
                </div>
            </div>
        </div>
        <div class="mt-3">
            <h4 class="text-uppercase text-center text-white">HD Casino Asia</h4>
            <p class="text-center text-white-50 font-14">We assist you to seek out the simplest Online Casinos and Sports Betting!<br>
                Easy to play games and bet to win real money with licensed, legal, and legit online casinos and sports betting websites for Asia players.<br>
                Follow us to urge more with exclusive bonuses</p>
        </div>
    </div>
@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>
@endsection
