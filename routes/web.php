<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// define global variable
view()->share('version', time());
view()->share('host_name', 'HD Casino Asia');

// Views
Route::get('/', 'HomeController@index')->name('home');
Route::get('/1xbet', 'OnexbetController@index')->name('1xbet');
Route::get('/188bet', 'One88betController@index')->name('188bet');
Route::get('/855play', 'Eight55playController@index')->name('855play');
Route::get('/22bet', 'Two2betController@index')->name('22bet');

// Auth
Auth::routes();

// Admin group
Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function()
{
    Route::get('/', function() {
        return redirect('/admin/dashboard');
    });
    Route::get('dashboard', 'admin\DashboardController@index')->name('dashboard');

    // User
    Route::get('user', 'admin\UserController@index')->name('user');
});
